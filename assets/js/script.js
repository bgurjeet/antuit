$('document').ready(function() {
	// *****code for hamburger starts here********
	$('nav').click(function() { 
		var ham = $('.navigation');

		if(!ham.hasClass('animate')) {
			$(ham).removeClass('animate2');
			$(ham).addClass('animate');	
			$('.first-ham').addClass('rotate1');
			$('.second-ham').addClass('rotate2');
			$('.third-ham').addClass('rotate3');
		}
		else {
			$(ham).removeClass('animate');
			$(ham).addClass('animate2');
			$('.first-ham').removeClass('rotate1');
			$('.second-ham').removeClass('rotate2');
			$('.third-ham').removeClass('rotate3');
		}

});

	



	// ******code for hamburger ends here****

  // ************** code for team tab starts here **********

  var all = $('.all');
  var leader = $('#leader');


  $(leader).addClass('design');
  $('.leader').addClass('active');
  $(leader).click(function() {
  	$(all).removeClass('active');
  	$('.leader').addClass('active');
  	$('span').removeClass('design');
  	$(this).addClass('design');
  });
  $('#management').click(function() {
  	$(all).removeClass('active');
  	$('.management').addClass('active');
  	$('span').removeClass('design');
  	$(this).addClass('design');
  });
  $('#advisors').click(function() {
  	$(all).removeClass('active');
  	$('.advisors').addClass('active');
  	$('span').removeClass('design');
  	$(this).addClass('design');
  });
  $('#center').click(function() {
  	$(all).removeClass('active');
  	$('.center').addClass('active');
  	$('span').removeClass('design');
  	$(this).addClass('design');
  });
  // *************** code for team tab ends here************

  // ************code for slider starts here***********


  $('.services-list').slick({
  	infinite: true,
  	slidesToShow: 1,
  	slidesToScroll: 1,
  	mobileFirst:true,
  	responsive:[
  	{
  		breakpoint: 900,
  		settings: {
  			slidesToShow: 3,
  			slidesToScroll: 3,
  			infinite: true
  		}
  	}
  	]
  });


  // *************code for slider ends here**************
  // ************* code for tab starts here********

  $('.tabs-container > div:first').addClass('display');
  $('.show li').click(function(){
		// $('.show span.done').removeClass('done');
		// $(this).addClass('done');
		var selectTab = $(this).attr('id'); 		
		$('.tabs-container > div').removeClass('display');
		$('.tabs-container > div'+selectTab).addClass('display');
	});



	// ************** code for tabs ends here************
	// ***********code for form validation starts here*********
	$('#email').blur(function() {
		var email = $('#email').val();
		var emailreg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
		if (email.match(emailreg) && email.length !== 0) {
			$('.error').removeClass('enter');
		} else {
			$('.error').addClass('enter');
		}
	});

	$('button').click(function(){
		var email = $('#email').val();
		if (email.length==0) {
			$('.error').addClass('enter');
		}
	});
	// *************	code for form validation ends here*******
	// **********scroll to top code *********
	$(window).scroll(function(){
		if($(window).scrollTop() > 200){
			$('.scroll').addClass('move');
		}
		else {
			$('.scroll').removeClass('move');	
		}
	});
	$('.scroll').click(function(e){
		e.preventDefault();
		$('html,body').animate({scrollTop:0},'500');	
	});
});